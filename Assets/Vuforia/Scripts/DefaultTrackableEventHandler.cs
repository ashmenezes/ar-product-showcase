/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class DefaultTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
        #region PRIVATE_MEMBER_VARIABLES
 
        private TrackableBehaviour mTrackableBehaviour;
        #endregion // PRIVATE_MEMBER_VARIABLES

		//public Transform target;
		public bool MaxMin;
		public Transform EndPoint;
		public Transform Child;
		//public Transform TextTargetName;

		//public Transform NavMenu; 
		private Vector3 DefaultPoint;//
		private float[] SmoothedRotation = new float[4]{0,0,0,0};//
		private float[] RotationVels  = new float[4]{0.0f,0.0f,0.0f,0.0f};
		public Vector3 DefaultScale;
		//private Vector3 DefaultScaleNavMenu;
		private Vector3 velocity = Vector3.one;
		private Vector3 Childelocity = Vector3.one;
		//private Vector3 NavMenulocity = Vector3.zero;

        #region UNTIY_MONOBEHAVIOUR_METHODS
    
        void Awake()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
			SmoothedRotation = new float[4]{0,0,0,0};
			if (Child.parent) {
				Child.parent = null;
			}
			DefaultScale = Child.localScale;
			//DefaultScaleNavMenu = NavMenu.localScale;
        }

		void Update () {



			if (Child) {
				if (MaxMin) {
					Child.position = Vector3.SmoothDamp (Child.position, EndPoint.position, ref velocity, 0.05f);
					Child.localScale = Vector3.SmoothDamp (Child.localScale, DefaultScale, ref Childelocity, 0.05f);


				} else {
					Child.localScale = Vector3.SmoothDamp (Child.localScale, Vector3.zero, ref Childelocity, 0.1f);

//					BottomPanel1.gameObject.SetActive(false);
				}
				SmoothedRotation [0] = Mathf.SmoothDamp (SmoothedRotation [0], EndPoint.rotation.x, ref RotationVels [0], 0.05f);
				SmoothedRotation [1] = Mathf.SmoothDamp (SmoothedRotation [1], EndPoint.rotation.y, ref RotationVels [1], 0.05f);
				SmoothedRotation [2] = Mathf.SmoothDamp (SmoothedRotation [2], EndPoint.rotation.z, ref RotationVels [2], 0.05f);
				SmoothedRotation [3] = Mathf.SmoothDamp (SmoothedRotation [3], EndPoint.rotation.w, ref RotationVels [3], 0.05f);
				Child.rotation = new Quaternion (SmoothedRotation [0], SmoothedRotation [1], SmoothedRotation [2], SmoothedRotation [3]);

			}
		} 

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
			if (Child) {
				Child.position = EndPoint.position;
				Child.transform.Translate (0, -6, 0);
			}
		  
			if (GetComponent<Product> ()) {
				GetComponent<Product> ().Scanned ();
			} else {
				Debug.LogWarning (gameObject.name + " doesn't have LinkedObject.cs");
			}

			MaxMin = true;




            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
        }


        private void OnTrackingLost()
        {
			if (GetComponent<Product> ()) {
				GetComponent<Product> ().LOST ();
			} else {
				Debug.LogWarning (gameObject.name + " doesn't have LinkedObject.cs");
			}
			MaxMin = false;
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");

//			BottomImage.gameObject.SetActive(false);//It seems BottomImage is not present in all objects, fix it
//			TopPanel.gameObject.SetActive(false);//I guess for other vars the same error
//			BottomPanel.gameObject.SetActive(false);//SetActive second time, why?

			//It is NOT recommended to do such stuff HERE
			//Maybe in AppManager.cs is better, is it? 


        }

        #endregion // PRIVATE_METHODS
    }
}
