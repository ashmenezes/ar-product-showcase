﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OreintationManager : MonoBehaviour {

	public GridLayoutGroup GLD;

	public Vector2 ForLand;

	public Vector2 ForPort;

	// Use this for initialization
	void Start () {
		GLD = GetComponent<GridLayoutGroup> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight) {
			UseLandscapeLeftLayout();
		}
		else if (Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown) {
			UsePortraitLayout();
		}

	}

	public void UseLandscapeLeftLayout(){
		GLD.cellSize = ForLand;
	}

	public void UsePortraitLayout(){
		GLD.cellSize = ForPort;
	}
}
