﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpandBubble : MonoBehaviour {

	[System.Serializable]
	public class Bubble
	{
		public GameObject Body;
		public Transform TargetPos;
		public Transform StartPos;
		public Transform EndPos;
		public Vector3 vel = Vector3.zero;
		public Vector3 velSize = Vector3.zero;
	}

	public List<Bubble> Bubbles = new List<Bubble> ();

	public bool Expand;
	//public GameObject Blocker;
	public float TargetSize = 50f;
	public float CurrentSize = 100f;
	private float vel = 0.0f;

	void Start() {
		ExapandColapse ();
	}

	void Update () {
		CurrentSize = Mathf.SmoothDamp (CurrentSize, TargetSize, ref vel, 0.1f);
		for (int i = 0; i < Bubbles.Count; i++) {
			Bubbles [i].Body.transform.localPosition = Vector3.SmoothDamp (Bubbles [i].Body.transform.localPosition, Bubbles [i].TargetPos.localPosition, ref Bubbles [i].vel, 0.1f);
			Bubbles [i].Body.GetComponent<RectTransform> ().sizeDelta = new Vector2 (CurrentSize, CurrentSize);
		}
	}

	public void ExapandColapse() {
		//Blocker.SetActive (Expand);
		Expand = !Expand;
		if (Expand) {
			TargetSize = 185;
		} else {
			TargetSize = 125;
		}
		for (int i = 0; i < Bubbles.Count; i++) {
			if (Expand) {
				Bubbles [i].TargetPos = Bubbles [i].EndPos;
				Bubbles [i].Body.GetComponent<Button> ().interactable = true;
			} else {
				Bubbles [i].TargetPos = Bubbles [i].StartPos;
				Bubbles [i].Body.GetComponent<Button> ().interactable = false;
			}
		}
	}

	public void ExpandForPage(int Page) {
		Bubbles [Page].TargetPos = Bubbles [Page].EndPos;
		Bubbles [Page].Body.GetComponent<Button> ().interactable = true;
	}

}
