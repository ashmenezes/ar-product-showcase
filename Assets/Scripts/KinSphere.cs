﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinSphere : MonoBehaviour {

	public float Timer;
	float d_v;
    public float speed = 0;
    GameObject target;
    bool Expanded;
    public Vector3 oldPos;
    Vector3 vel;

	// Use this for initialization
	void Start() {
		d_v = 0.5f + speed;
		target = GameObject.Find(gameObject.name + " (1)");
		if (oldPos == Vector3.zero) {
			oldPos = transform.localPosition;
			Destroy(target.GetComponent<MeshRenderer>());
			Destroy(target.GetComponent<MeshFilter>());
		}
	}
	
	// Update is called once per frame
	void Update () {
        if (Expanded) // If Expanded
        {
            //Contract
			transform.localPosition = Vector3.SmoothDamp(transform.localPosition, oldPos, ref vel, 0.5f);
        }
        else // If Contracted
        {
            //Expand
			transform.localPosition = Vector3.SmoothDamp(transform.localPosition, target.transform.localPosition, ref vel, 0.5f);
        }
		if (Timer > 0) {
			Timer -= Time.deltaTime;
		} else {
			Expanded = !Expanded;
			Timer = d_v;
		}
	}
}
