﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimOnScanned : MonoBehaviour {

	public Animator AC;
	public string NameOfAnim = "Active";

	public void PlayAnim() {
		Debug.Log ("Playing... " + gameObject.name);
		AC.Play (NameOfAnim);
	}
}
