﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Increment : MonoBehaviour {

	public float Delay;
	public float Timer;
	public float MoveAmount;
	public int Max;
	public int Current;
	public Vector3 Direction;
	Vector3 initlaPos;

	void Start() {
		initlaPos = transform.localPosition;

	}

	void Update () {
		Timer -= Time.deltaTime;
		if (Timer <= 0) {
			Timer = Delay;
			Current++;
			transform.Translate (Direction.x*MoveAmount,0,0);
			if (Current >= Max) {
				Current = 0;
				transform.localPosition = initlaPos;
			}
		}
	}
}
