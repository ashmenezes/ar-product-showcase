﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalSection : MonoBehaviour {

	public int Section;
	public float TargetX;
	public float TargetValue;
    //public float Offset = 592f;

    //New Variables
	public Transform EndPoint;
	public Transform StartPoint;
	private Vector3 TargetPos;
    private Vector3 curvel;
    public bool Show;

	private float vel = 0.0f;

	void Start () {
		ChangeSection (0);
        StartCoroutine(ShowUI());
	}

	void LateUpdate () {
        TargetValue = Mathf.SmoothDamp(TargetValue, TargetX, ref vel, 0.1f);
        //transform.localPosition = new Vector3(TargetValue, transform.localPosition.y, transform.localPosition.z);
		if (EndPoint && StartPoint){
			if (Show) {
				transform.localPosition = Vector3.SmoothDamp( transform.localPosition, EndPoint.localPosition, ref curvel, 0.1f);
			} else {
				transform.localPosition = Vector3.SmoothDamp( transform.localPosition, StartPoint.localPosition, ref curvel, 0.1f);
			}
        } else {
            transform.localPosition = new Vector3(TargetValue, transform.localPosition.y, transform.localPosition.z);
        }
    }

    IEnumerator ShowUI()
    {
        yield return new WaitForSeconds(1f);
        Show = true;
    }

	public void SwitchShow() {
		Show = !Show;
	}

	public void SwitchShowSpecified(bool Show) {
		this.Show = Show;
	}

    public void ChangeSection(int Section) {
		this.Section = Section;
		TargetX = Section * -1280f;
	}
}
