﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicChangeOfColor : MonoBehaviour {

	public Renderer MyRenderer;
	public GameObject LightPrefab;
	public Vector3 TargetPos;
	public List <Transform> Neighbors = new List<Transform> ();
	public float MaxDistance = 0.2f;
	public float Offset;
	public float Rows;
	float OffsetDefault;
	float X;
	public float MaxPoint;
	public float ResultOfDivision;
	[Range(0f,1f)]
	public float g = 0.839f;
	[Range(0f,1f)]
	public float b;

	public Vector3 vel = Vector3.zero;

	void Start () {
		OffsetDefault = Offset;
		MyRenderer = GetComponent<Renderer> ();
		if (Rows > 0) {
			for(int i = 0;i<=Rows;i++ ) {
				GameObject GO = Instantiate (LightPrefab,this.transform.position,Quaternion.identity,this.transform.parent);
				GO.transform.Translate (1.5f*i,0,0);
				GO.GetComponent <DynamicChangeOfColor> ().Offset = i+Offset;
			}
		}
		TargetPos = transform.localPosition;
	}

	void Update () {
		Color c = MyRenderer.material.color;
		if (Offset > 0) {
			Offset -= Time.deltaTime*4;
		} else {
			X += Time.deltaTime*4;
			TargetPos.y = Mathf.Sin (X)*3f;
			if (X > 200) {
				X = 0;
			}
		}
		transform.localPosition = Vector3.SmoothDamp (transform.localPosition,TargetPos,ref vel,1);
		ResultOfDivision = Mathf.Abs(transform.localPosition.y) / MaxDistance;
		//MyRenderer.material.color =new Color (Mathf.Abs( ResultOfDivision),b,g,1);
	}
}
