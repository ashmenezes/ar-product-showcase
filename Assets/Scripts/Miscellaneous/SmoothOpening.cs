﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothOpening : MonoBehaviour {

	public float Timer;
	public Color myColor;

	float Limit = 1;
	float ResponseTime = 0.1f;
	float Speed = 0.5f;
	Vector3 vel = Vector3.zero;
	Vector3 target;
	Vector3 InitialPos;
	Renderer myRenderer;

	float D_R = 0;
	float D_G = 0;
	float D_B = 0;

	void Start() {
		InitialPos = transform.localPosition;
		target = new Vector3 (transform.localPosition.x,transform.localPosition.y,-0.0077f);
		myRenderer = GetComponent <Renderer> ();
		D_R = Random.Range (0.07f, 0.08f);
		D_G = D_R;
		D_B = D_R;
	}

	void LateUpdate () {
		if (Timer >= ResponseTime) {
			transform.localPosition = Vector3.SmoothDamp (transform.localPosition, target, ref vel, Speed);
		} else {
			transform.localPosition = Vector3.SmoothDamp (transform.localPosition, InitialPos, ref vel, Speed);
		}
		float Limit = transform.localPosition.z*this.Limit;
		float ColorLimit = Limit / target.z;
		float tempR = 0;
		float tempG = 0;
		float tempB = 0;
		tempR = ColorLimit * myColor.r;
		tempG = ColorLimit * myColor.g;
		tempB = ColorLimit * myColor.b;
		if (ColorLimit < 0.1f) {
			tempR = D_R;
			tempG = D_G;
			tempB =  D_B;
		}
		myRenderer.material.color = new Color (tempR, tempG, tempB, 1);
		if (Timer > 0) {
			Timer -= Time.deltaTime*2;
		} else {
			Timer = 0;
		}
	}

	void Move(float Limit = 1) {
		this.Limit = Limit;
		target = new Vector3 (transform.localPosition.x,transform.localPosition.y,-0.0077f*Limit);
		Timer += Time.deltaTime*4;
		if (Timer >= ResponseTime) {
			Timer = ResponseTime;
		}
	}
}
