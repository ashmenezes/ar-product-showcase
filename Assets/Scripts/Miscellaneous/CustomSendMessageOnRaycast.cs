﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomSendMessageOnRaycast : MonoBehaviour {

	public string Message = "Move";
	public float Limit = 1;

	void Start() {
		Limit = Random.Range (0.75f,1f);
		Destroy (GetComponent <MeshRenderer>());
		Destroy (GetComponent <BoxCollider>());
		Destroy (GetComponent <MeshFilter>());
	}

	void Update () {
		RaycastHit hit;
		if (Physics.Raycast (transform.position, transform.forward, out hit)) {
			hit.collider.SendMessage (Message,Limit);
		}
	}
}
