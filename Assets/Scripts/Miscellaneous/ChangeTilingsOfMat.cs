﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTilingsOfMat : MonoBehaviour {

	public Vector2 Pos;
	public float SpeedFactor = 5;
	public bool Move;

	void Update () {
		Pos.x -= Time.deltaTime/SpeedFactor;
		if (Pos.x <= -1) {
			Pos.x = 0;
		}
		GetComponent <Renderer>().sharedMaterial.mainTextureOffset = Pos;
	}

	public void  SetPos() {

	}
}
