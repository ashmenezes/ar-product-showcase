﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Product : MonoBehaviour {

	public GameObject TitleField;
	public GameObject LinkedObject;
	public Sprite Icon;
	public string ProductName;
	public string VideoURL;
	[TextArea(3,3)]
	public string Desc;
	public Product[] RelatedProducts;


	public void Scanned () {
		TitleField.SetActive (true);
		if (LinkedObject) {
			LinkedObject.SetActive (true);
		}
		Debug.Log ("Scanned");
		TitleField.GetComponentInChildren<Text>().text = ProductName;
		GameObject.FindObjectOfType<AppManager> ().DisplayProductByID (this);
		GameObject.FindObjectOfType<AppManager> ().DisplayProductsByID (this);
		if (GetComponent<PlayAnimOnScanned> ()) {
			GetComponent<PlayAnimOnScanned> ().PlayAnim ();
		}
	}

	public void LOST() {
		TitleField.SetActive (false);
		if (LinkedObject) {
			LinkedObject.SetActive (false);
		}
	}
}
