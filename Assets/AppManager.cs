﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppManager : MonoBehaviour {

//	[System.Serializable]
//	public class Product
//	{
//		public string Name;
//		public int ID;
//		public string URL;
//		[TextArea(4,7)]
//		public string Desc;
//		public Sprite Icon;
//
//	}

	//private Button MyButton = null;
	// Use this for initialization
	public GameObject InfoPanel;
	public GameObject RelatedButton;

	public Button ScrollViewButton;
	public Button VideoButton;
	public Button InfoButton;
	public Transform Content;
	public Product[] Products;

	void Start() {
		Products = FindObjectsOfType<Product> ();
	}

	public void OpenURL(string url){
		Application.OpenURL(url);
	}

	public void DisplayProductByID (Product tempProduct) {
		Debug.Log ("Displaying a product " + tempProduct.ProductName);
		ScrollViewButton.onClick.RemoveAllListeners ();
		VideoButton.onClick.RemoveAllListeners ();
		CleanList ();
		ScrollViewButton.onClick.AddListener (() => DisplayProductsByID (tempProduct));
		VideoButton.onClick.AddListener (() => OpenURL (tempProduct.VideoURL));
		if (tempProduct) {
			InfoPanel.GetComponentInChildren<Text> ().text = tempProduct.Desc;
		} else {
			InfoPanel.GetComponentInChildren<Text> ().text = "Missing product";
		}
	}

	public void DisplayProductsByID (Product tempProduct){
		CleanList ();
		Debug.Log ("Displaying products of " + tempProduct.ProductName);
		foreach (var item in tempProduct.RelatedProducts) {
			Debug.Log ("Spawning " + item.name);
			GameObject GO = Instantiate (RelatedButton, Content.position, Quaternion.identity, Content);
			GO.GetComponentsInChildren<Image> ()[1].sprite = item.Icon;
			Text[] texts = GO.GetComponentsInChildren<Text> ();
			texts[0].text = item.ProductName;
			texts[1].text = item.Desc;
			GO.GetComponent<Button>().onClick.AddListener (() => OpenURL (item.VideoURL));
		}
	}

	public void CleanList() {
		Image[] tempProducts = Content.GetComponentsInChildren<Image> ();
		foreach (var item in tempProducts) {
			Destroy (item.gameObject);
		}
	}

	public void UpdateVideoButton(string Url) {
		VideoButton.onClick.RemoveAllListeners ();
		VideoButton.onClick.AddListener (() => OpenURL (Url));
	}

//	public void SwitchSections() {
//		HorizontalSection HS = FindObjectOfType<HorizontalSection> ();
//		if (HS.Section == 0) {
//			HS.ChangeSection (-1);
//		} else {
//			HS.ChangeSection (0);
//		}
//	}

	Product GetProductByID(string name = "missing") {
		foreach (var item in Products) {
			if (item.ProductName == name) {
				return item;
			}
		}
		return null;
	}

}
